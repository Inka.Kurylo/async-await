"use ctrict";
import { requestIP} from './requestIP.js';
import{ getAddress} from './getAddress.js';
import {createElementEvent } from "./createElementEvent.js";

let text;
(async ()=>{
    const ipAddress = await requestIP();
    text = await getAddress(ipAddress);
    let continent = text.timezone.split('/');
    createElementEvent(`Continent: ${continent[0]},<br> Country: ${text.country},<br>  City: ${text.city},<br>  Region: ${text.regionName}`)
})();  

