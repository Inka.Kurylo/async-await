
export async function requestIP(){
    let response = await fetch('https://api.ipify.org/?format=json')
    const {ip} = await response.json();
    return ip;
}
 